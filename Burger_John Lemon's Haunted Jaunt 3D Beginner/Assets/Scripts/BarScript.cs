﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarScript : MonoBehaviour
{
    public ProgressBar bar;

    private float value;

    // Start is called before the first frame update
    void Start()
    {
        value = 50f;
    }

    // Update is called once per frame
    void Update()
    {
        if(value > 0f)
        {
            value -= 2 * Time.deltaTime;
        }
        else
        {
            value = 0f;
        }

        bar.BarValue = value;
    }

    public float getValue()
    {
        return value;
    }
    public void addValue(float v)
    {
        value += v;
    }
}
