﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LemonBoy : MonoBehaviour
{

    public NavMeshAgent m_navMeshAgent;
    public Transform[] waypoints;
    int m_CurrentWaypointIndex;
    Animator m_Animator;

    // Start is called before the first frame update
    void Start()
    {
        m_navMeshAgent.SetDestination(waypoints[0].position);
        m_Animator = GetComponent<Animator>();
        m_Animator.SetBool("IsWalking", true);
    }

    // Update is called once per frame
    void Update()
    {
        if (m_navMeshAgent.remainingDistance < m_navMeshAgent.stoppingDistance)
        {
            m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Length;
            m_navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
        }
    }
}
