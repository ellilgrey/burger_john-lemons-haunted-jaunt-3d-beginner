﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostMovement : MonoBehaviour
{
    public float turnSpeed = 20f;
    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    CapsuleCollider m_collider;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;
    public float speed = .1f;
    public GameObject m_sphere;
    public ProgressBar invisBar;
    static public float invisValue;
    public static float invis = 10;
    public static float invisDrop = 5f;
    public static float invisDropHold = 1f;

    private float leftBound = -29f;
    private float rightBound = 19f;
    private float topBound = 16f;
    private float bottomBound = -14f;

    // Start is called before the first frame update
    void Start()
    {
        invisValue = 50f;
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
        m_collider = GetComponent<CapsuleCollider>();
    }

    void FixedUpdate()
    {
        Move();
        invisValue -= invisDrop * Time.deltaTime;
        _updateBar();

        if(Input.GetButton("Space") && GhostMovement.invisValue > 0f)
        {
            m_collider.isTrigger = true;
            m_sphere.SetActive(true);
            GhostMovement.invisValue -= invisDropHold;
        }
        else
        {
            m_collider.isTrigger = false;
            m_sphere.SetActive(false);
        }

        clamp();
    }

    private void Move()
    {
        //set the horizontal and vertical axis
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        //set the movement Vector
        m_Movement.Set(horizontal, 0f, vertical);

        //normalize the vector
        m_Movement.Normalize();

        //setting a bool to whether there is horizontal input or not
        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);

        //setting a bool to whether there is vertical input or not
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);

        //combine the two bools
        bool isWalking = hasHorizontalInput || hasVerticalInput;

        //sets the bool on the animator
        m_Animator.SetBool("IsWalking", isWalking);

        if (isWalking)
        {
            //moving the animation in the desired direction
            m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * speed);

            //moving rotation
            m_Rigidbody.MoveRotation(m_Rotation);
        }
        //sets the forward vector
        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed *
            Time.deltaTime, 0f);

        //stores the rotation
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }

    public static void updateBar()
    {
        invisValue += invis;
    }

    private void _updateBar()
    {
        invisBar.BarValue = invisValue;
    }

    private void clamp()
    {
        Vector3 clampedPosition = transform.position;
        clampedPosition.x = Mathf.Clamp(clampedPosition.x, leftBound, rightBound);
        clampedPosition.z = Mathf.Clamp(clampedPosition.z, bottomBound, topBound);
        clampedPosition.y = transform.position.y;

        transform.position = clampedPosition;
    }
}
