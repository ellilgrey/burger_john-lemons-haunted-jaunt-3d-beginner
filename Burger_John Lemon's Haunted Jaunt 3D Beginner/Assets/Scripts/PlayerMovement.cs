﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;
    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;

    // Start is called before the first frame update
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //set the horizontal and vertical axis
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        //set the movement Vector
        m_Movement.Set(horizontal, 0f, vertical);

        //normalize the vector
        m_Movement.Normalize();

        //setting a bool to whether there is horizontal input or not
        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);

        //setting a bool to whether there is vertical input or not
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);

        //combine the two bools
        bool isWalking = hasHorizontalInput || hasVerticalInput;

        //sets the bool on the animator
        m_Animator.SetBool("IsWalking", isWalking);

        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        //sets the forward vector
        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed *
            Time.deltaTime, 0f);

        //stores the rotation
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }

    private void OnAnimatorMove()
    {
        //moving the animation in the desired direction
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * 
            m_Animator.deltaPosition.magnitude);

        //moving rotation
        m_Rigidbody.MoveRotation(m_Rotation);
    }
}
